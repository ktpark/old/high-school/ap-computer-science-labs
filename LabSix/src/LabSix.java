import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * @author Kevin Park
 * Date: 10/20/2014
 * Period: 3
 * 
 * Class that calculate and output the basic statistics for
 * a file of integer. [average, standard deviation, and mode(s)].
 * integers will range between 0 and 100.
 */
public class LabSix {
	public static void main(String[]args) {
		int[] nums = readInt("numbers.txt"); //Extract the numbers from the file
		System.out.println("Average: "+getAvg(nums)); //Calculate the average and print
		System.out.println("Standard Deviation: "+getStdDeviation(nums)); //Calculate the standard deviation and print
		System.out.print("Mode(s): "); //Prepares to print modes
		int[] mode = getMode(nums); //Calculate modes
		for(int i=0;i<mode.length;i++)
			System.out.print(mode[i]+" "); //Prints all the modes
	}
	/**
	 * Takes in a file name and extracts integers into an integer array
	 * 
	 * @param fileName String containing the name of the file that the numbers will be extracted from
	 * @return Integer array that contains all the numbers from the file
	 */
	public static int[] readInt(String fileName) {
		int[] nums = new int[0]; //Creates an empty array that will be increased
		try {
			File inF = new File(fileName); //Create a File object for numbers file
			Scanner in = new Scanner(inF); //Create a Scanner object to read numbers
			while(in.hasNextInt()) { //Repeat until there aren't any more numbers
				int[] temp = new int[nums.length+1]; //Create a temp array with incremented space
				for(int i=0;i<nums.length;i++) //copy existing numbers to temp
					temp[i] = nums[i];
				temp[nums.length] = in.nextInt(); //Place the new number into the extra space at temp
				nums = temp; //Copy temp to nums
			}
			in.close(); //Close the scanner
		} catch (FileNotFoundException e) {
			System.err.println("File Not Found: "+fileName ); //When the file is not found, print this
		}
		return nums; //return the result
	}
	/**
	 * Takes in array of integer and calculates the average and returns as an double
	 * 
	 * @param nums Integer array to be calculated for the average
	 * @return Double consisting the average of the nums
	 */
	public static double getAvg(int[] nums) {
		int sum = 0; //where the numbers will be added
		for(int i=0;i<nums.length;i++) //Add up all the numbers
			sum+=nums[i];
		return (double)sum/nums.length; //Divide the sum by number of nums and return
	}
	/**
	 * Takes in array of integer and calculates the standard deviation and returns as an double
	 * 
	 * @param nums Integer array to be calculated for the standard deviation
	 * @return Double consisting the standard deviation of the nums
	 */
	public static double getStdDeviation(int[] nums) {
		double avg = getAvg(nums); //Get the average of numbers
		double num = 0; //Where the standard deviation will be stored
		for(int i=0;i<nums.length;i++)
			num+=(nums[i]-avg)*(nums[i]-avg); //number minus average and square it and add to the num
		num = num/(nums.length-1); //divide the result from the previous statement by 1 minus of the length
		num = Math.sqrt(num); //Square root the num and get standard deviation
		return num; //return the answer
	}
	/**
	 * Takes in array of integer and calculates mode(s) and returns as integer arrays
	 * 
	 * @param nums Integer array to be calculated for mode
	 * @return Array of integer that will be returned containing modes
	 */
	public static int[] getMode(int[]nums) {
		int[] freq = new int[101]; //Place to store the frequency of 1-100
		for(int i=0;i<101;i++) { //Calculates how many times a number repeats and stores it
			for(int j=0;j<nums.length;j++)
				if(nums[j]== i)
					freq[i]++;
		}
		int large = 0; //Where the most repeated amount will be stored
		for(int i=0;i<freq.length;i++) { //Calculates for the most repeated amount
			if(large<freq[i])
				large = freq[i];
		}
		int[] mode = new int[0]; //Where the modes will be stored
		if(large ==0) //To protect from an array of length 0
			return mode;
		for(int i=0;i<freq.length;i++) { //Find and stores for any numbers that match the most repeated amount
			if(freq[i]== large) {
				int[] temp = new int[mode.length+1];
				for(int j=0;j<mode.length;j++)
					temp[j] = mode[j];
				temp[mode.length] = i;
				mode = temp;
			}
		}
		return mode; //return the modes
	}
}
