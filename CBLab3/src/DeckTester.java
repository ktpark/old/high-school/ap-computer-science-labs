/**
 * This is a class that tests the Deck class.
 */
public class DeckTester {
	
	/**
	 * The main method in this class checks the Deck operations for consistency.
	 *	@param args is not used.
	 */
	public static void main(String[] args) {
		String[] ranks = {"jack", "queen", "king"};
		String[] suits = {"blue", "red"};
		int[] pointValues = {11, 12, 13};
		Deck d = new Deck(ranks, suits, pointValues);

		System.out.println("**** Original Deck Methods ****");
		System.out.println("  toString:\n" + d.toString());
		System.out.println("  isEmpty: " + d.isEmpty());
		System.out.println("  size: " + d.size());
		System.out.println();
		System.out.println();

		System.out.println("**** Deal a Card ****");
		System.out.println("  deal: " + d.deal());
		System.out.println();
		System.out.println();

		System.out.println("**** Deck Methods After 1 Card Dealt ****");
		System.out.println("  toString:\n" + d.toString());
		System.out.println("  isEmpty: " + d.isEmpty());
		System.out.println("  size: " + d.size());
		System.out.println();
		System.out.println();

		System.out.println("**** Deal Remaining 5 Cards ****");
		for (int i = 0; i < 5; i++) {
			System.out.println("  deal: " + d.deal());
		}
		System.out.println();
		System.out.println();

		System.out.println("**** Deck Methods After All Cards Dealt ****");
		System.out.println("  toString:\n" + d.toString());
		System.out.println("  isEmpty: " + d.isEmpty());
		System.out.println("  size: " + d.size());
		System.out.println();
		System.out.println();

		System.out.println("**** Deal a Card From Empty Deck ****");
		System.out.println("  deal: " + d.deal());
		System.out.println();
		System.out.println();

		
		String[] rank = {"ace","king","queen","jack","10","9", "8","7","6","5","4","3","2"};
		String[] suit = {"spades","hearts","diamonds","clubs"};
		int[] pointValue = {11,10,10,10,10,9,8,7,6,5,4,3,2};
		Deck deck = new Deck(rank,suit,pointValue);
		
		System.out.println("**** Deal 5 Cards and reshuffle ****");
		System.out.println("  deal: " + deck.deal());
		System.out.println("  deal: " + deck.deal());
		System.out.println("  deal: " + deck.deal());
		System.out.println("  deal: " + deck.deal());
		System.out.println("  deal: " + deck.deal());
		System.out.println();
		System.out.println("  toString:\n" + deck.toString());
		deck.shuffle();
		System.out.println("  toString:\n" + deck.toString());

		/* *** TO BE COMPLETED IN ACTIVITY 4 *** */
	}
	 
	/*
	public static void main(String[] args) {
		int[] point = {2,1,6};
		Deck deck = new Deck(new String[]{"A", "B", "C"},new String[]{"Giraffes", "Lions"}, point);
		System.out.println(deck);
		System.out.println(deck.isEmpty());
		System.out.println(deck.size());
		
		deck.deal();
		System.out.println(deck);
		System.out.println("\nCLEAR");
		while(!deck.isEmpty())
			System.out.println(deck.deal());
		System.out.println(deck.isEmpty());
	}
	*/
}
