/**
 * This is a class that tests the Card class.
 */
public class CardTester {

	/**
	 * The main method in this class checks the Card operations for consistency.
	 *	@param args is not used.
	 */
	public static void main(String[] args) {
		Card card1 = new Card("King","Diamond", 11);
		Card card2 = new Card("Queen","Spade", 10);
		Card card3 = new Card("Ace","Heart", 1);
		Card card4 = new Card("King","Diamond", 11);
		
		System.out.println("Card 1: "+card1);
		System.out.println("Card 2: "+card2);
		System.out.println("Card 3: "+card3);
		System.out.println("Card 1 is same as Card 4: "+card1.matches(card4));
	}
}
