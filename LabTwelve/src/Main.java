import info.gridworld.actor.*;

public class Main {
	public static void main(String[]args) {

		ActorWorld w = new ActorWorld();
		Flower d = new Flower();
		w.add(d);
		Rock c = new Rock();
		w.add(c);
		//CrabCritter r = new MachoCrab();
		//w.add(r);
		Critter b = new BashfulCritter(5);
		w.add(b);
		w.show();
	}
}
