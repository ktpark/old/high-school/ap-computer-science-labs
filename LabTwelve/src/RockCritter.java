import java.util.ArrayList;
import info.gridworld.actor.Actor;
import info.gridworld.actor.Critter;
import info.gridworld.actor.Rock;
import info.gridworld.grid.Location;

public class RockCritter extends Critter{
	public void processActors(ArrayList<Actor> actors) {
		for(int i=0;i<actors.size();i++) {
			if(actors.get(i) instanceof Rock)
				getGrid().remove(new Location(actors.get(i).getLocation().getRow(), actors.get(i).getLocation().getCol()));
		}
	}
}
