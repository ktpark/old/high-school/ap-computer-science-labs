import java.util.ArrayList;

import info.gridworld.actor.Actor;
import info.gridworld.actor.Rock;
import info.gridworld.grid.Location;

public class MachoCrab extends CrabCritter{
	public void processActors(ArrayList<Actor> actors) {
		for(int i=0;i<actors.size();i++) {
			int x = getGrid().getNumCols();
			int y = getGrid().getNumRows();
			int x2 = actors.get(i).getLocation().getCol();
			int y2 = actors.get(i).getLocation().getRow();
			int x1 = getLocation().getCol();
			int y1 = getLocation().getRow();
			//System.out.println("Actor: "+x2+", "+y2);
			//System.out.println("Crit: "+x1+", "+y1);
			Location newL = new Location( y2-(y1-y2), x2-(x1-x2));
			if(newL.getCol()<0||newL.getCol()>=x||newL.getRow()<0||newL.getRow()>=y)
				actors.get(i).removeSelfFromGrid();
			else {
				if(getGrid().get(newL) instanceof Rock)
					actors.get(i).removeSelfFromGrid();
				else
					actors.get(i).moveTo(newL);
			}
		}
	}
}
       