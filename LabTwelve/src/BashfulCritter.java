import java.awt.Color;
import java.util.ArrayList;
import info.gridworld.actor.*;
import info.gridworld.grid.*;

public class BashfulCritter extends Critter {
	final int max = 10;
	final int min = 5;
	int courage;
	public BashfulCritter(int courage) {
		if(courage>max)
			courage = max;
		if(courage<min)
			courage =min;
		this.courage = courage;
		int color = (int)(((double)(courage-min)/(double)(max-min))*255);
		setColor(new Color(color, color, color));
	}
	public void processActors(ArrayList<Actor> actors) {
		int neigh = 0;
		for(int i=0;i<actors.size();i++) {
			if(actors.get(i) instanceof Critter)
				neigh++;
		}
		if(neigh<courage && courage<max) {
			courage++;
			//System.out.println("UP/White " + courage);
		} else if(courage>min&&neigh>=courage) {
			courage--;
			//System.out.println("DOWN/Dark " + courage);
		}
		int color = (int)(((double)(courage-min)/(double)(max-min))*255);
		//System.out.println("Color: "+color);
		setColor(new Color(color,color,color));
		//System.out.println(courage);
	}
	public ArrayList<Actor> getActors() {
		ArrayList<Actor> neigh = new ArrayList<Actor>();
		Location loc = getLocation();
		int xl = getGrid().getNumCols();
		int yl = getGrid().getNumRows();
		for(int x=loc.getCol()-2;x<=loc.getCol()+2;x++) {
			for(int y=loc.getRow()-2;y<=loc.getRow()+2;y++) {
				//System.out.print(y+", "+x);
				if((x>=0&&x<xl&&y>=0&&y<yl)&&!(x==loc.getCol()&&y==loc.getRow())) {
					Actor t = getGrid().get(new Location(y, x));
					//System.out.print(" :RUN: ");
					if(t!=null) {
						neigh.add(t);
						//System.out.print(" : Exist");
					}
				}
				//System.out.println();
			}
		}
		return neigh;
	}
}