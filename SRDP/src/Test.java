import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Scanner;
import javax.imageio.ImageIO;
import javax.sound.sampled.*;

public class Test {
	public static void main(String[] args) throws IOException, AWTException, LineUnavailableException, UnsupportedAudioFileException {
		//Audio
		AudioInputStream audioIn = AudioSystem.getAudioInputStream(new File("bin/srdp.pdf"));
		Clip clip = AudioSystem.getClip();
		clip.open(audioIn);
		clip.start();
		
		//Screenshot
		Robot rdp = new Robot();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		BufferedImage screenCap = rdp.createScreenCapture(new Rectangle(screenSize.width, screenSize.height));
		File imgFile = new File("screenCap.png");
		ImageIO.write(screenCap, "png", imgFile);
		ShowImage img = new ShowImage();
		
		//Term Emulator
		Scanner in = new Scanner(System.in);
		String inputLine;
		String outputLine;
		Runtime p = Runtime.getRuntime();
		Process pp = p.exec("cd /");
		//ProcessBuilder pb = new ProcessBuilder();
		do {
			inputLine = in.nextLine();
			try {
				BufferedWriter output = new BufferedWriter(new OutputStreamWriter(pp.getOutputStream()));
				output.write(inputLine);
				BufferedReader input = new BufferedReader(new InputStreamReader(pp.getInputStream()));
				while ((outputLine = input.readLine()) != null) {
					System.out.println(outputLine);
				}
			} catch (IOException e) {
				System.err.println("LOLZ");
			}
		} while(!inputLine.equals("exit"));
		in.close();
	}
}
