import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
/**
 * @author Kevin Park
 * Date: 11/19/2014
 * Period: 3
 * 
 * This is an erasing program that will erase any point that is connected to the user input coordinate
 */
public class LabEight {
	private static Scanner input = new Scanner(System.in); //Console input
	private static char[][] canvas; //2D array canvas
	public static void main(String[]args) {
		File f;
		do {
			System.out.print("Enter the file name: ");
			f = new File(input.nextLine()); //Initialize file with the name that the user specified
		} while(!f.exists()); //Loop if the file does not exist
		try {
			read(new Scanner(f)); //Read the 
			while(true) {
				int choice = prompt();
				if(choice==1)
					print(canvas);
				else if(choice==2)
					erasePrompt();
				else
					System.exit(0);
			}
		} catch (FileNotFoundException e) {
			System.err.println("File Not Found: "+f.getName());
			System.exit(1);
		}
	}
	/**
	 * This method will read the file specified by the user and put it into an array
	 * 
	 * @param file Scanner object that is specified to a file to be read
	 */
	public static void read(Scanner file) {
		canvas = new char[20][20];
		for(int i=0;i<canvas.length;i++) {
			for(int j=0;j<canvas[i].length;j++) {
				canvas[i][j] = '-';
			}
		}
		while(file.hasNextInt()) {
			canvas[file.nextInt()-1][file.nextInt()-1] = '@';
		}
		file.close();
	}
	/**
	 * This method prompts and returns what the user want to do with the program
	 * 
	 * @return It returns an integer of what the user selected ranging from 1-3
	 */
	public static int prompt() {
		System.out.println("\n1. Display\n2. Erase\n3. Exit ");
		int choice;
		do {
			System.out.print("Choose your options: ");
			choice=input.nextInt();
		} while(choice<1||choice>3);
		return choice;
	}
	/**
	 * This method will prompt the user on which coordinate to remove
	 * and pass it on to the erase method
	 */
	public static void erasePrompt() {
		int x,y;
		System.out.println();
		do {
			System.out.print("X Coordinate: ");
			x = input.nextInt()-1;
		} while(x<0||x>19);
		do {
			System.out.print("Y Coordinate: ");
			y = input.nextInt()-1;
		} while(y<0||y>19);
		erase(x, y);
	}
	/**
	 * This method will erase the point on the canvas as well as connecting points.
	 * It is the same thing as the bucket removing.
	 * 
	 * @param x Contains the x coordinate
	 * @param y Contains the y coordinate
	 */
	public static void erase(int x, int y) {
		if(x<0||x>19||y<0||y>19)
			return;
		else if(canvas[y][x]!='@')
			return;
		else {
			canvas[y][x]='-';
			erase(x+1, y);
			erase(x-1, y);
			erase(x, y+1);
			erase(x, y-1);
		}
		
	}
	/**
	 * This method will print what is currently stored in the 2D array
	 * 
	 * @param can Contains the 2D canvas (can = shortened version of canvas)
	 */
	public static void print(char[][] can) {
		System.out.println("\n   12345678901234567890");
		for(int i=0;i<can.length;i++) {
			System.out.printf("%2d ", i+1);
			for(int j=0;j<can[i].length;j++) {
				System.out.print(can[i][j]);
			}
			System.out.println();
		}
	}
}
