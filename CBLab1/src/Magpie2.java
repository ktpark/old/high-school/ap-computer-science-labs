import java.util.StringTokenizer;

/**
 * A program to carry on conversations with a human user.
 * This is the initial version that:  
 * <ul><li>
 *       Uses indexOf to find strings
 * </li><li>
 * 		    Handles responding to simple words and phrases 
 * </li></ul>
 * This version uses a nested if to handle default responses.
 * @author Laurie White
 * @version April 2012
 */
public class Magpie2
{
	/**
	 * Get a default greeting 	
	 * @return a greeting
	 */
	public String getGreeting()
	{
		return "Hello, let's talk.";
	}
	
	/**
	 * Gives a response to a user statement
	 * 
	 * @param statement
	 *            the user statement
	 * @return a response based on the rules given
	 */
	public String getResponse(String statement)
	{
		String response = "";
		//Activity 3 Tester
		/*System.out.println(findKeyword("She's my sister", "sister", 0));
		System.out.println(findKeyword("Brother Tom is helpful", "brother", 0));
		System.out.println(findKeyword("I can't catch wild cats.", "cat", 0));
		System.out.println(findKeyword("I know nothing about snow plows.", "no", 0));*/
		
		if(statement.trim().toLowerCase().startsWith("i want"))
			response = "Would you really be happy if you had "+statement.trim().substring(7).replaceAll("\\.", "").trim()+"?";
		else if (statement.trim().toLowerCase().startsWith("i")&&statement.trim().toLowerCase().replaceAll("\\.", "").endsWith("you")) {
			String trimmed = statement.replaceAll("\\.", "").trim();
			response = "Why do you "+trimmed.substring(2, trimmed.length()-3).trim()+" me?";
		}
		else if (statement.indexOf("no") >= 0)
		{
			response = "Why so negative?";
		}
		else if (findKeyword(statement,"mother", 0) >= 0
				|| findKeyword(statement,"father",0) >= 0
				|| findKeyword(statement,"sister",0) >= 0
				|| findKeyword(statement,"brother",0) >= 0)
		{
			response = "Tell me more about your family.";
		} else if(findKeyword(statement,"dog",0)>=0
				||findKeyword(statement,"cat",0)>=0)
			response = "Tell me more about your pets";
		else if(findKeyword(statement,"Ms. Nguyen",0)>=0)
			response = "She sounds like a good teacher.";
		else if(statement.trim().length()==0)
			response = "Say something, please.";
		else if(findKeyword(statement,"can",0)>=0)
			response = "I don't know. Can you?";
		else if(findKeyword(statement,"website",0)>=0)
			response = "Error 404. Website not found.";
		else if(findKeyword(statement,"clock",0)>=0)
			response = "Tic tok. It's procrasinate o'clock.";
		else
		{
			response = getRandomResponse();
		}
		return response;
	}

	/**
	 * Pick a default response to use if nothing else fits.
	 * @return a non-committal string
	 */
	private String getRandomResponse()
	{
		final int NUMBER_OF_RESPONSES = 10;
		double r = Math.random();
		int whichResponse = (int)(r * NUMBER_OF_RESPONSES);
		String[] moreResponses = {"Yes", "No", "Maybe", "So what?"};
		String response = "";
		
		if (whichResponse == 0)
		{
			response = "Interesting, tell me more.";
		}
		else if (whichResponse == 1)
		{
			response = "Hmmm.";
		}
		else if (whichResponse == 2)
		{
			response = "Do you really think so?";
		}
		else if (whichResponse == 3)
		{
			response = "You don't say.";
		}
		else if (whichResponse == 4)
		{
			response = "I don't care.";
		}
		else if (whichResponse == 5)
		{
			response = "Gotcha, mate.";
		}
		else
			response = moreResponses[whichResponse - 6];

		return response;
	}
	/**
	 * Find the exact matches of the keyword, instead of cases where the keyword is embedded in a longer word
	 * @param statement The phrase that the method will look inside
	 * @param goal The keyword that its looking for
	 * @param startPos Start index of the search
	 * @return The index where the keyword starts. If it doesn't exist return -1.
	 */
	public int findKeyword(String statement, String goal, int startPos) {
		statement = statement.trim().toLowerCase().replaceAll("\\.", "");
		StringTokenizer st = new StringTokenizer(statement);
		int index = 0;
		while(st.hasMoreTokens()) {
			String word = st.nextToken();
			if(word.equalsIgnoreCase(goal) || word.equalsIgnoreCase(goal+"s"))
				return index;
			index+=word.length()+1;
		}
		return -1;
	}
}
