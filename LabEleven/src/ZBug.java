import java.awt.Color;
import info.gridworld.actor.*;
import info.gridworld.grid.*;

public class ZBug extends Bug{
	private int size;
	private int i = 0;
	private boolean turn = false;
	private boolean done = false;
	public ZBug(int size) {
		this.size = size;
		setColor(Color.RED);
		setDirection(getDirection() + Location.EAST);
	}
	public ZBug(int size, Color bugColor)
    {
		this.size = size;
        setColor(bugColor);
		setDirection(getDirection() + Location.EAST);
    }
	public void act()
    {
        boolean can = i<size;
			if (canMove()&&can) {
		        i++;
				move();
			}
			else
				if(!done) {
					i=0;
					if(!turn) {
						turn();
						turn();
						turn();
						turn = true;
					} else {
						turn();
						turn();
						turn();
						turn();
						turn();
						done =true;
					}
				}
    }
}
