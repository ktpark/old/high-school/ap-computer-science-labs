import info.gridworld.actor.Actor;
import info.gridworld.actor.ActorWorld;
import info.gridworld.grid.*;

public class BugRunner {
	public static void main(String[]args) {
		//Grid un = new UnboundedGrid();
		ActorWorld w = new ActorWorld();
		w.setGrid(new UnboundedGrid<Actor>());
		SpiralBug sb = new SpiralBug(3);
		w.add(sb);
		ZBug zb = new ZBug(4);
		w.add(zb);
		int[] t = {3, 3, 2, 1};
		DancingBug db= new DancingBug(t);
		w.add(db);
		w.show();
	}
}
