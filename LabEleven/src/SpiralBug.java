import java.awt.Color;
import info.gridworld.actor.*;

public class SpiralBug extends Bug{
	private int turn;
	private int i = 0;
	public SpiralBug(int startLength) {
		setColor(Color.RED);
		turn = startLength;
	}
	public SpiralBug(Color bugColor, int startLength)
    {
        setColor(bugColor);
		turn = startLength;
    }
	public void act()
    {
		boolean can = i<turn;
        if (canMove()&&can) {
            move();
            i++;
        } else {
    		i=0;
    		turn++;
            turn();
            turn();
        }
    }
}
