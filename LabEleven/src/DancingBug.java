import java.awt.Color;
import info.gridworld.actor.*;

public class DancingBug extends Bug{
	private int turn[];
	private int number = 0;
	private int i = 0;
	public DancingBug(int[] turn) {
		setColor(Color.RED);
		this.turn = turn;
	}
	public DancingBug(Color bugColor, int[] turn)
    {
        setColor(bugColor);
        this.turn = turn;
    }
	public void act()
    {	
		boolean can;
        if(turn[i]==number)
        	can = true;
        else
        can = false;
        if (canMove()&&can) {
            move();
            number=0;
            if(i==turn.length-1)
            	i=0;
            else
            	i++;
        }
        else {
    		number++;
            turn();
        }
    }
}