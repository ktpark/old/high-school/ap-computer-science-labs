import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * 
 * @author Kevin Park
 * Date: 11/10/2014
 * Period: 3
 * 
 * Program that will simulate Conway's Game of Life. It is a computer simulation
 * of the life and death events of a population of bacterial organisms.
 * 
 */

public class LabSeven {
	static boolean[][] world = new boolean[20][20];
	public static void main(String[]args ) {
		Scanner input= new Scanner(System.in);	//Scanner for the user inputs
		System.out.print("Please enter the file name: ");
		File life = new File(input.nextLine());	//File of the user inputs.
		Scanner finput;	//Scanner for the file
		try {
			finput = new Scanner(life);	//Initialize the Scanner for the file
			while(finput.hasNextInt()) {	//Checks if there are more numbers to read
				int row = finput.nextInt();	//Reads the first number in a line which indicates the row
				int col = finput.nextInt();	//Reads the second number in a line which indicates the column
				world[row-1][col-1] = true;	//Set the coordinates in the array to true
			}
			finput.close(); //Close the file Scanner
		} catch (FileNotFoundException e) {
			System.err.println("File not found: "+life.getName());	//Prints if the file does not exists
			System.exit(1);
		}
		printWorld(); //Prints the current world
		int gen; //Variable for the number of generations
		do {	//Loop for the generation input just in case it's negative number
			System.out.print("\nEnter the generations: " );
			gen = input.nextInt();	//Reads the number of generations
		} while (gen<0);
		input.close();
		doGenerations(gen);		//Executes the certain amount of generations
		printWorld();	//Print the result
	}
	/**
	 * This method prints the current state of the array (grid).
	 */
	public static void printWorld() {
		int row =0 , col =0 , all = 0;	//Variables for the number of organisms in row 10, column 10, and the entire gird
		System.out.println("\n     12345678901234567890\n");	//Sets up for printing
		for(int i=0;i<20;i++) {	//Loop rows
			System.out.printf("%2d   ", i+1);	//Prepares for printing for each line
			for(int j=0;j<20;j++) {	//Loop Columns
				if(world[i][j]) {	//Prints if the coordinates returns true
					System.out.print("*");
					if(i==9)
						row++;	//Increment for the number of organisms in row 10
					if(j==9)
						col++;	//Increment for the number of organisms in column 10
					all++;	//Increment for the number of organisms in the entire gird
				} else
					System.out.print(" "); //Prints space for places where organisms does not exists
			}
			System.out.println(); //New line for the next line
		}
		System.out.println("\nNumber in Row 10: "+row);	//Prints the stats in row 10
		System.out.println("Number in Column 10: "+col);	//Prints the stats in column 10
		System.out.println("Number of living organisms: "+all);	//Prints the stats in the entire grid
	}
	/**
	 * This method executes the number of generations that has been typed in.
	 * Every empty cell with three living neighbors will come to life in the next generation.
	 * Any cell with one or zero neighbors will die of loneliness, while any cell with four or more neighbors will die from overcrowding.
	 * Any with two or three neighbors will live into the next generation.
	 * @param num Number of generations to be executed
	 */
	public static void doGenerations(int num) {
		for(int i=0;i<num;i++) {	//Loops the number of generations
			boolean[][] temp = new boolean[20][20];	//Create a temp array for the new generations
			for(int r=0;r<world.length;r++) {	//Loop rows
				for(int c=0;c<world[r].length;c++) {	//Loop columns
					int n = numNeighbor(r,c);	//Gets the number of neighbor in the position
					if(n<=1||n>=4)	//Death for 1 or less and 4 or more neighbors
						temp[r][c] = false;
					else if(n==3)	//Birth for 3 neighbors
						temp[r][c] = true;
					else
						temp[r][c] = world[r][c];	//Same for 2 neighbors
				}
			}
			world = temp; //Make the new generation the current generation
		}
	}
	/**
	 * This method will read the certain point of the array and return the number of neighbors.
	 * @param row Row of the array
	 * @param col Column of the array
	 * @return Number of neighbors in the position of row and column
	 */
	public static int numNeighbor(int row, int col) {
		int sum=0;	//Variable where the number of neighbors will be stored
		//Conditions for the positions in the corner 
		if(row!=0)	//Position above
			if(world[row-1][col])
				sum++;
		if(row!=19)	//Position below
			if(world[row+1][col])
				sum++;
		if(col!=0)	//Position left
			if(world[row][col-1])
				sum++;
		if(col!=19)	//Position right
			if(world[row][col+1])
				sum++;
		if(row!=0&&col!=0)	//Position above-left
			if(world[row-1][col-1])
				sum++;
		if(row!=0&&col!=19)	//Position above-right
			if(world[row-1][col+1])
				sum++;
		if(row!=19&&col!=19)	//Position below-right
			if(world[row+1][col+1])
				sum++;
		if(row!=19&&col!=0)	//Position below-left
			if(world[row+1][col-1])
				sum++;
		return sum;	//return the number of neighbors
	}
}