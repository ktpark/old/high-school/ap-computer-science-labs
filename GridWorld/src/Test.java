import java.util.ArrayList;
import info.gridworld.actor.*;

public class Test {

	public static void main(String[] args) {
		ActorWorld w = new ActorWorld();
		ArrayList<BoxBug> bb = new ArrayList<BoxBug>();
		for(int i=0;i<20;i++) {
			bb.add(new BoxBug(5));
			w.add(bb.get(i));
		}
		w.show();
		/*
		 * Inheritance - The ability to inhert public methods to its current own class
		 * Polymorphism - Child will act like it defines itself
		 * Parent Class (Super Class) - The class being inherted
		 * Child Class (Subclass) - The class that is being inherted to
		 * Root of Hierarchy - Uppermost parent class
		 * Overriding a Method - The ability to forcefully replace the current existing method
		 */
	}

}
