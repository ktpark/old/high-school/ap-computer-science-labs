import info.gridworld.actor.*;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

public class FirstProject {
	public static void main(String[]args) {
		ActorWorld world = new ActorWorld();
		Actor duck = new Actor();
		world.add(duck);
		duck.moveTo(new Location(5, 5));
		for(int i=0;i<10;i++) {
			Actor d = new Actor();
			world.add(d);
			d.moveTo(new Location(5, i));
		}
		world.show();
	}
}
