
public class Test {
	public static void main(String[] args){
		int[] arr = {1,2,3,4,5,6,7,8,9,10,11};
		EnhancedArray t = new EnhancedArray();
		System.out.println(t.isEmpty());
		for(int i:arr)
			t.add(i);
		t.add(11, 102);
		t.add(11, 102);
		t.add(11, 102);
		t.add(4, 12);
		t.add(4, 12);
		t.add(4, 12);
		t.add(10, 7);
		t.add(10, 7);
		t.add(10, 7);
		t.add(2, 3);
		t.add(2, 3);
		t.add(2, 3);
		for(int i=0;i<t.mySize;i++)
			System.out.println(t.get(i));
		System.out.println(t.isEmpty());
		t.set(0, 999);
		for(int i=0;i<t.mySize;i++)
			System.out.println(t.get(i));
	}
}
