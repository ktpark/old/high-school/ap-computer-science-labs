/**
 *	A class that represents an array that is easier to manipulate.
 *  An EnhancedArray can easily add, remove, get and set elements while
 *  updating its own size as necessary.
 *
 *  @author: Kevin Park
 */
public class EnhancedArray{

	public static int[] myArray; //the array with all of the integer elements in this collection
	public static int mySize;


	/**
	* Constructs an empty EnhancedArray
	*/
	public EnhancedArray(){
		mySize = 0; //Set size
		myArray = new int[10]; //Create new array
	}


	/**
	* Appends the specified element to the end of this array.
	*
	* @param element - the element to be added to array
	*/
	public void add(int element){
		if(myArray.length > mySize) { //If there is space
			myArray[mySize] = element; //set new element
			mySize++; //Increment the size
		} else {
			int[] temp = new int[myArray.length*2]; //Double the size if it runs out of space
			for(int i=0;i<myArray.length;i++) { //Copy and paste data
				temp[i] = myArray[i];
			}
			temp[mySize] = element; //set the new data
			myArray = temp; //Set alias
			mySize++; //Increment the size
		}
		/*int[] temp = new int[mySize + 1];
		for(int i = 0; i < myArray.length; i++)
			temp[i] = myArray[i];
		temp[mySize] = element;
		myArray = temp;*/
	}


	/**
	* Inserts the specified element to a particular position in the array.
	*
	* @param index - the position the element will be added to
	* @param element - the element that will be added to the array.
	*/
	public void add(int index, int element){
		if(index < 0||index>mySize)
			throw new ArrayIndexOutOfBoundsException();
		if(myArray.length > mySize) { //if there is space
			for(int i=mySize-1;i>=index;i--) //Shift the datas to create space
				myArray[i+1] = myArray[i]; 
			myArray[index] = element; //Set new data
			mySize++; //Increment size
		} else {
			int[] temp = new int[myArray.length*2]; //Double the size if no space available
			for(int i=0;i<myArray.length;i++) { //Copy and paste
				temp[i] = myArray[i];
			}
			for(int i=mySize-1;i>=index;i--) //Shift data
				temp[i+1] = temp[i];
			temp[index] = element; //Set new data
			myArray = temp; //Set alias
			mySize++; //Increment size
		}
		/*if(index < myArray.length - 1 && mySize != 0){
			int[] temp = new int[mySize + 1];
			for(int i = 0; i < index; i++)
				temp[i] = myArray[i];
			for(int i = index; i < myArray.length; i++)
				if(i != index)
					temp[i+1] = myArray[i];
			temp[index] = element;
			myArray = temp;
		}*/
	}


	/**
	* Returns the element at the specified location. Does not change original array.
	*
	* @param index - the position where the element will be retrieved
	* @return the element at the given index
	*/
	public int get(int index){
		return myArray[index]; //Return data
	}


	/**
	*	Returns true if this list contains no elements.
	*
	* @return true if there are no elements, false otherwise
	*/
	public boolean isEmpty(){
		return mySize==0; //Return true if size equals 0 meaning empty
	}


	/**
	* Removes a particular element from the array.
	*
	* @param index - the index of the element to be removed
	* @return the element that has been removed from the array
	*/
	public void remove(int index){
		if(index < mySize && mySize != 0){ //Checks if the parameters are valid
			mySize --;  //Decrements mySize
			for(int i=index;i<mySize;i++) //Shifts to the left
				myArray[i] = myArray[i+1];
			/*int[] temp = new int[myArray.length-1];
			for(int i = 0; i < index; i++)
				temp[i] = myArray[i];
			for(int i = index; i < temp.length; i++)
					temp[i] = myArray[i+1];
			myArray = temp;*/
		} else
			throw new ArrayIndexOutOfBoundsException();
	}


	/**
	* Replaces the element at the specified position in this array with the specified element.
	*
	* @param index - the position of the element to be replaced
	* @param element - the element that will be used to replace the other
	* @return the element that has been replaced
	*/
	public void set(int index, int element){
		if(index<mySize&&index>=0) //Checks if index is valid
			myArray[index] = element; //Returns number at index
		else
			throw new ArrayIndexOutOfBoundsException();
	}

	/**
	* Returns the number of elements in this array.
	*
	* @return the number of elements in the list
	*/
	public int size(){
		return mySize; //returns size
	}

}