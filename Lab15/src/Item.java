/**
 * A class that represents an item from a store. Contains an 
 * id number and inventory (the # of items left in the store)
 * 
 * @author Kevin Park
 */
public class Item implements Comparable<Item>{
	/**
	 * ID number for the item (each is unique)
	 */
	private int myId;

	/**
	 * Number of items left in the store
	 */
	private int myInv;

	/**
	 * Creates an item with a specific id and inventory
	 * @param id - the unique ID number for this item
	 * @param inv - the number of this item in stock
	 */
	public Item(int id, int inv){
		myId = id; //Set the global id with param
		myInv = inv; //Set the global inv with param
	}

	/**
	 * Returns the ID number
	 * @return the ID number for this item
	 */
	public int getId(){ 
		return myId; 
	}

	/**
	 * Returns the number of this item in store
	 * @return the number of items in the store
	 */
	public int getInv(){
		return myInv;
	}

	/**
	 * Returns a negative number when the calling item
	 * is before the argument item (according to ID number),
	 * 0 if the IDs are the same, and a positive number otherwise
	 * @param other the item to compare this item to
	 */
	public int compareTo(Item other){ 
		return myId-other.getId();
		//If myId is before other than returns negative number
		//If myInv is before other than returns negative number
	}

	/**
	 * Returns whether these items are the same or not (ID numbers)
	 * @param other the item to compare to 
	 * @return true if the items have the same ID number, false if not
	 */
	public boolean equals(Item other){
		if(other.getId()==myId)
			return true;
		return false;
	}

	/**
	 * Returns the String representation of this item
	 */
	public String toString(){
		return String.format("%10d%10d", myId, myInv); //formats with 10 spaces for each variable
	}
}
