import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;
/**
 * A class that represents a Store containing
 * some set of items
 * 
 * @author Kevin Park
 */
public class Store{

	/**
	 * List of Item objects; represents the items currently 
	 * present in this store
	 */
	private ArrayList<Item> myStore;

	/**
	 * Creates a store with the items lists in the given file
	 * @param fName the name of the file containing the items of the store
	 */
	public Store(String fName){ 
		myStore = new ArrayList<Item>();
		loadFile(fName); //Load custom file
	}

	/**
	 * Creates a store with the items in the file "file50.txt"
	 */
	public Store(){
		myStore = new ArrayList<Item>();
		loadFile("file50.txt"); //Load default file
	}

	/**
	 * Loads the store with the elements in the given file
	 * @param inFileName the file to be read that contains the items in the store
	 */
	private void loadFile(String inFileName){ 
		try {
			BufferedReader in = new BufferedReader(new FileReader(inFileName)); //Reader that will be used to read file
			String t; //Temporary variable to store a line from the file
			while((t = in.readLine())!=null) { //Continue until the end of the file
				StringTokenizer st = new StringTokenizer(t);
				int t1 = Integer.parseInt(st.nextToken()); //Separate 1st integer
				int t2 = Integer.parseInt(st.nextToken()); //Separate 2nd integer
				myStore.add(new Item(t1, t2)); //Add new item to the Arraylist
				//System.out.println(t1+" "+t2);
			}
			in.close(); //Close the file reader
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Display each item the store with the given format: 
	 * Number		ID Number		Inventory
	 * #			#				#
	 * For readability, an empty line appears every 10 items
	 * and list is shown in lined-up columns
	 */
	public void displayStore(){
		System.out.println(this); //Print out the formatted string
	}

	/**
	 * Returns the String representation of this store
	 */
	public String toString(){
		String t = ""; //Where it will be stored
		System.out.println(String.format("%10s%10s%10s\n", "Number", "Id", "Inv")); //Header
		for(int i=1;i<=myStore.size();i++) { //Loop through the every Items
			t+=String.format("%10d%s\n", i, myStore.get(i-1)); //Append numbering
			if(i%10==0)
				t+="\n"; //Newline every ten integer
		}
		return t; //return String
	}  

	/**
	 * Sorts the store by ID number
	 */
	public void sort(){
		mergeSort(myStore, 0, myStore.size()-1); //Sort items
	}	

	/**
	 * Merge two sublists of Items
	 * @param a	the list containing the two sublists
	 * @param first  starting index of range of values to be sorted
	 * @param mid    midpoint index of range of values to be sorted
	 * @param last   last index of range of values to be sorted
	 */
	private void merge(ArrayList<Item> a, int first, int mid, int last){
		ArrayList<Item> t = (ArrayList)a.clone(); //Clone original as helper
		int f=first, l=mid+1, c=first; //Counter variables
		while(f<=mid&&l<=last) { //While both are not empty
			if(t.get(f).compareTo(t.get(l))>0) { //Compare two from each array
				a.set(c,t.get(l)); //One from 2nd array is smaller
				l++; //Increment the 2nd array counter
			} else {
				a.set(c, t.get(f)); //One from 1st array is smaller
				f++; //Increment the 1st array counter
			}
			c++; //Increment positioning counter
		}
		while(f<=mid) { //Fill in remainders from 1st array
			a.set(c, t.get(f));
			//One for a.set() and one for t.get()
			f++; c++;
		}
	}

	/**
	 * Mergesort the list of items
	 * @param a the list containing all the items to be sorted
	 * @param first the index of the first element in the list to be sorted
	 * @param last the index of the last element in the list to be sorted
	 */
	private void mergeSort(ArrayList<Item> a, int first, int last){ 
		if(last-first!=0) { //Stop recursive when it's by itself
			mergeSort(a, first, first+(last-first)/2); //if 0-3 -> 0-1, 2-3 //0-2 -> 0,1 2,2
			mergeSort(a, (first+(last-first)/2)+1, last); //4-6 -> 4-5, 6,6 //3-5 -> 3,4 5,5
			merge(a, first, first+(last-first)/2, last); //Merge two together
		}
	}
	
	public void testSearch(){
		int idToFind;
		int invReturn;
		int index;
		Scanner in = new Scanner(System.in);
		System.out.println("Testing search algorithm\n");
		do{
			System.out.println();
			System.out.print("Enter Id value to search for (-1 to quit) ---> ");
			idToFind = in.nextInt();
			//index = bsearch(new Item(idToFind, 0));
			//recursive version call
			index = bsearch (new Item(idToFind, 0), 0, myStore.size()-1);
			System.out.print("Id # " + idToFind);
			if (index == -1){
				System.out.println(" No such part in stock");
			}else{
				System.out.println(" Inventory = " + myStore.get(index).getInv());
			}
		} while (idToFind >= 0);
	}
	/**
	 * Searches the myStore ArrayList of Item Objects for the specified
	 * item object using a iterative binary search algorithm
	 *
	 * @param idToSearch Item object containing id value being searched for
	 * @return index of Item if found, -1 if not found
	 */
	private int bsearch(Item idToSearch){
		int first=0,last=myStore.size()-1,index=myStore.size()/2; //Position Locator variable
        while(last>=first) { //Loop until it runs out of space
        	if(idToSearch.compareTo(myStore.get(index))<0) {  //If id comes before half
    			last=index-1; //Set a new range
    			index=first+(index-first)/2; //Set middle
    		} else if(idToSearch.compareTo(myStore.get(index))>0) { //If id comes after half
    			first=index+1; //Set a new range
    			index=index+(last-index)/2+1; //Set middle
    		} else 
                return index; //return value when it matches
            //System.out.println(first+" "+index+" "+last);
        }
        return -1; //Return -1 when it didn't find anything
	}
	/**
	 * Searches the specified ArrayList of Item Objects for the specified
	 * id using a recursive binary search algorithm
	 *
	 * @param idToSearch Id value being search for
	 * @param first Starting index of search range
	 * @param last Ending index of search range
	 * @return index of Item if found, -1 if not found
	 */

	private int bsearch(Item idToSearch, int first, int last){
		if(last<first)  //When out of range
			return -1; //Return -1
		int index = first+(last-first)/2; //Calculate middle
		//System.out.println(first+" "+index+" "+last);
    	if(idToSearch.compareTo(myStore.get(index))<0) //When id is smaller
    		return bsearch(idToSearch, first, index-1); //New range (first)~(index-1)
    	else if(idToSearch.compareTo(myStore.get(index))>0) //When id is larger
    		return bsearch(idToSearch, index+1, last); //New range (index+1)~(last)
    	else
    		return index; //return index when found
	}
}
