import java.io.File;
import java.util.Scanner;
import java.io.IOException;

/**
 *  Reads data from a text file about a maze and can find the
 *  solutions to the maze
 *
 * @author     Administrator
 * @created    July 16, 2002
 *
 * Modified by Jason Quesenberry and Nancy Quesenberry
 * February 10, 2006
 */
public class ThreadTheMaze{
  /**
   *  Description of the Field
   */
  private final static char BLANK = ' ';
  private static final int MAXROW = 12;
  private static final int MAXCOL = 12;
  private char [][] myMaze;
  private int myMaxRow, myMaxCol;

  public ThreadTheMaze(){
	myMaze = new char [MAXROW + 1][MAXCOL + 1];
  	myMaxRow = myMaze.length - 1;
  	myMaxCol = myMaze[0].length - 1;
  }
  /**
   *  Initiates the trace process
   *
   * @param  none
   */
  public void doTraceMaze() {
  	loadMaze();
  	traceMaze(myMaze, myMaxRow/2, myMaxCol/2);
  }
  /**
   *  Loads the maze characters from mazeData.txt
   */
  private void loadMaze(){
  	String line;
  	Scanner in;
  	try{
  		in = new Scanner(new File("mazeData.txt"));

    	for (int row = 1; row <= myMaxRow; row++){
      		line = in.nextLine();
      		for (int col = 1; col <= myMaxCol; col++){
        		myMaze[row][col] = line.charAt(col-1);
      		}
    	}
    }catch(IOException i){
    	System.out.println("Error: " + i.getMessage());
    }
  }

  /**
   *  Prints the maze to the console.
   *
   * @param  maze  maze to be printed
   */
  public void printMaze(char[][] maze){
    Scanner console = new Scanner(System.in);

    for (int row = 1; row <= myMaxRow; row++){
      for (int col = 1; col <= myMaxCol; col++){
        System.out.print("" + maze[row][col]);
      }
      System.out.println();
    }
    System.out.println();
    System.out.println("Hit enter to see if there are more solutions.");
    String anything = console.nextLine();
    
  }

  /**
   *  Will attempt to find a path out of the maze.  A path will
   *  be marked with the ! marker.  The method makes a copy of
   *  the array each time so that only the path out will be
   *  marked, otherwise extra ! markers will appear in the answer.
   *  The function is recursive.
   *
   * @param  maze  the maze to be considered/solved
   * @param  row   the row of current position
   * @param  col   the column of current position
   */
  public void traceMaze(char[][] maze, int row, int col){
	//I need to stop this path if:
    //printMaze(maze);
	//#1 if out of bounds --> FOUND THE SOLUTION
    if(row <=0||row>MAXROW||col<=0||col>MAXCOL)
    	printMaze(maze);
    //#2
    else if(maze[row][col] != BLANK) {
    	//do nothing
    }
    //#3 otherwise, try this step again
    else {
    	//mark spot:
    	maze[row][col] = '!';
    	traceMaze(maze, row-1, col);
    	traceMaze(maze, row+1, col);
    	traceMaze(maze, row, col-1);
    	traceMaze(maze, row, col+1);
    	
    	maze[row][col] = BLANK;
    }
  }

}
