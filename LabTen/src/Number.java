import java.util.ArrayList;
/** 
 * @author Kevin Park
 * Date: 12/11/2014
 * Period: 3
 * 
 * Represents a number using Arraylist of Digit and display them in different bases
 */
public class Number {
	ArrayList<Digit> num = new ArrayList<Digit>(); //Digit Array
	int base; //Base it is stored in
	/**
	 * Default constructor for Number class
	 * Sets the value to 0, but in base 10
	 */
	public Number() {
		num.add(new Digit()); //Create a digit with 0
		base = 10; //set the base to 10
	}
	/**
	 * Alternative constructor for Number class
	 * 
	 * @param number Number to be set for the first time
	 * @param base The base it should be converted to
	 */
	public Number(int number, int base) {
		int result;
		do {
			result = number/base; //result of division
			num.add(new Digit(number%base, base)); //set the digit with the remainder
			number = result; //set the number with result
		} while(result!=0);
		this.base = base; //set the base
	}
	/**
	 * Increments the number by 1 including carry bits
	 */
	public void increment() {
		boolean carry; //True if there is a carry
		carry = num.get(0).increment(); //See if there is a carry
		for(int i=0;i<num.size();i++) {
			if(carry) { //if there is a carry
				if(i==num.size()-1) //if there is no available place to store the carry
					num.add(new Digit(0, base)); //create a new digit
				carry = num.get(i+1).increment(); //check for carry again
			}
		}
	}
	/**
	 * Turns Number into String format for easy output
	 */
	public String toString() {
		String number = ""; //append here
		for(int i=num.size()-1;i>=0;i--)
			number+=num.get(i); //append higher digits first
		return number;
	}
}
