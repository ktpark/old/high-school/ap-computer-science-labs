/**
 * @author Kevin Park
 * Date: 12/11/2014
 * Period: 3
 * 
 * Represents one digit of a number in any base
 */
public class Digit {
	private char number; //Where digit is stored
	private int base; //Base it's in
	private int pos; //Position of num
	private char[] num= "0123456789abcdefghijklmnopqrstuvwxyz".toUpperCase().toCharArray(); //Helper numbers
	/**
	 * Default constructor for Digit class.
	 * Sets the number to 0 and the base to 10
	 */
	public Digit() {
		this.number = '0'; //Sets the number to 0
		this.base = 10; //Sets the base to 10
	}
	/**
	 * Alternative constructor for Digit class
	 * where user can set the variables.
	 * 
	 * @param number Takes in a number in the base 10 and convert it
	 * @param base Base of the number it should be converted to
	 */
	public Digit(int number, int base) {
		boolean possible = false; //checks if the number within the base
		for(int i=0;i<base;i++) {
			if(number == i) {
				possible = true;
				pos = i;
				break;
			}
		}
		if(!possible) {
			System.err.println("Number is not within the base!");
			System.exit(1);
		}
		this.number = num[pos]; //Sets the number to number
		this.base = base; //Sets the base to base
	}
	/**
	 * Increments the current value by 1
	 * 
	 * @return Returns true if the digit gets carried
	 */
	public boolean increment() {
		if(number == num[base-1]) { //See if it needs to carry
			number = '0'; //Reset
			pos = 0;
			return true; //Return true for carry
		}
		pos++; //Increment the position
		number = num[pos]; //Set the incremented number
		return false;
	}
	/**
	 * Converts the class into a printable String format
	 */
	public String toString() {
		return number+""; //Return the current value
	}
}
