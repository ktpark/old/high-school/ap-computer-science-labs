import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileReading {
	public static void main(String[]args) {
		try {
			File inF = new File("test1.txt");
			Scanner in = new Scanner(inF);
			while(in.hasNext())
				System.out.println(in.nextLine());
		} catch (FileNotFoundException e) {
			System.err.println("File Not Found: test1.txt");
			e.printStackTrace();
		}
	}
}
