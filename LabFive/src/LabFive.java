import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
/**
 * @author Kevin Park
 * Date: 10/20/2014
 * Period: 3
 * 
 * Class that will squeeze a file by taking out the initial blank
 * spaces at the beginning of each line and writing them as a number
 */
public class LabFive {
	static Scanner in;
	static File outF;
	static FileWriter out;
	public static void main(String[]args) {
		squeeze("squeeze.txt");
	}
	/**
	 * Method that takes a string containing file name and squeeze the
	 * file by taking out the initial blank spaces at the beginning of
	 * each line and writing them as a number in a new file appending
	 * NEW to the before the original file name
	 * @param fileName String that contains the name of the file
	 */
	public static void squeeze(String fileName) {
		try {
			in = new Scanner(new File(fileName)); //Scanner created to read the squeeze file
			outF = new File("NEW"+fileName); //File consisting the output file
			out = new FileWriter(outF); //Writer that is going to write to the new file
			outF.createNewFile(); //Create the new file
		} catch (FileNotFoundException e) {
			System.err.println("File not found: "+fileName); //Print error message for FileNotFoundException
			System.exit(1); //Exit
		} catch (IOException e) {
			System.err.println("Error while trying to create a new file: NEW"+fileName); //Print error message for IOException
			System.exit(1); //Exit
		}
		try {
			while(in.hasNext()) { //Checks if there is more line
				String temp = in.nextLine(); //Read the bnext line
				for(int i=0;i<temp.length();i++) { //Figure out the number of blank spaces
					if(temp.charAt(i)!=' ') { 
						//System.out.println(i+" "+temp.substring(i));
						out.write(i+" "+temp.substring(i)+"\n"); //Write to a new file
						break; //Break if there aren't any more blank spaces
					}
				}
			}
			in.close(); //Close files
			out.close();
		} catch (IOException e) {
			System.out.println("Error encountered while trying to modify the file: NEW"+fileName); //Throw Exception
		}
	}
}
