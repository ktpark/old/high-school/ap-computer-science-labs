/**
 * This class contains class (static) methods
 * that will help you test the Picture class 
 * methods.  Uncomment the methods and the code
 * in the main to test.
 * 
 * @author Barbara Ericson 
 */
public class PictureTester
{
	/** Method to test zeroBlue */
	public static void testZeroBlue()
	{
		Picture beach = new Picture("beach.jpg");
		beach.explore();
		beach.zeroBlue();
		beach.explore();
	}

	/** Method to test mirrorVertical */
	public static void testMirrorVertical()
	{
		Picture caterpillar = new Picture("caterpillar.jpg");
		caterpillar.explore();
		caterpillar.mirrorVertical();
		caterpillar.explore();
	}

	/** Method to test mirrorTemple */
	public static void testMirrorTemple()
	{
		Picture temple = new Picture("temple.jpg");
		temple.explore();
		temple.mirrorTemple();
		temple.explore();
	}

	/** Method to test the collage method */
	public static void testCollage()
	{
		Picture canvas = new Picture("640x480.jpg");
		canvas.createCollage();
		canvas.explore();
	}

	/** Method to test edgeDetection */
	public static void testEdgeDetection()
	{
		Picture swan = new Picture("swan.jpg");
		swan.edgeDetection(10);
		swan.explore();
	}

	/** Method to test keepOnlyRed */
	public static void testKeepOnlyRed()
	{
		Picture beach = new Picture("beach.jpg");
		beach.explore();
		beach.keepOnlyRed();
		beach.explore();
	}

	/** Method to test keepOnlyGreen */
	public static void testKeepOnlyGreen()
	{
		Picture beach = new Picture("beach.jpg");
		beach.explore();
		beach.keepOnlyGreen();
		beach.explore();
	}

	/** Method to test keepOnlyBlue */
	public static void testKeepOnlyBlue()
	{
		Picture beach = new Picture("beach.jpg");
		beach.explore();
		beach.keepOnlyBlue();
		beach.explore();
	}

	/** Method to test negate */
	public static void testNegate()
	{
		Picture beach = new Picture("beach.jpg");
		beach.explore();
		beach.negate();
		beach.explore();
	}

	/** Method to test grayscale */
	public static void testGrayscale()
	{
		Picture beach = new Picture("beach.jpg");
		beach.explore();
		beach.grayscale();
		beach.explore();
	}

	/** Method to test fixUnderwater */
	public static void testFixUnderwater()
	{
		Picture beach = new Picture("water.jpg");
		beach.explore();
		beach.fixUnderwater();
		beach.explore();
	}

	/** Method to test mirrorVerticalRightToLeft */
	public static void testMirrorVerticalRightToLeft()
	{
		Picture beach = new Picture("caterpillar.jpg");
		beach.explore();
		beach.mirrorVerticalRightToLeft();
		beach.explore();
	}

	/** Method to test mirrorHorizontal */
	public static void testMirrorHorizontal() {
		Picture beach = new Picture("caterpillar.jpg");
		beach.explore();
		beach.mirrorHorizontal();
		beach.explore();
	}

	/** Method to testmirrorHorizontalBotToTop */
	public static void testMirrorHorizontalBotToTop() {
		Picture beach = new Picture("caterpillar.jpg");
		beach.explore();
		beach.mirrorHorizontalBotToTop();
		beach.explore();
	}

	/** Method to test mirrorDiagonal */
	public static void testMirrorDiagonal() {
		Picture beach = new Picture("beach.jpg");
		beach.explore();
		beach.mirrorDiagonal();
		beach.explore();
	}

	/** Method to test mirrorArms */
	public static void testMirrorArms() {
		Picture snowman = new Picture("snowman.jpg");
		snowman.explore();
		snowman.mirrorArms();
		snowman.explore();
	}

	/** Method to test mirrorArms */
	public static void testMirrorGull() {
		Picture snowman = new Picture("seagull.jpg");
		snowman.explore();
		snowman.mirrorGull();
		snowman.explore();
	}

	/** Method to test copy v2 */
	public static void testCopy() {
		Picture canvas = new Picture("640x480.jpg");

		Picture arch = new Picture("arch.jpg");
		canvas.copy(arch, 0, 0, 37, 247, 104, 293);
		Picture beach = new Picture("beach.jpg");
		canvas.copy(beach, 350, 550, 145, 223, 402, 639);
		Picture mark = new Picture("blue-mark.jpg");
		canvas.copy(mark, 360, 300, 167, 303, 274, 385);
		canvas.explore();
	}
	
	/** Method to test myCollage */
	public static void testMyCollage() {
		Picture canvas = new Picture("640x480.jpg");
		canvas.myCollage();
		canvas.explore();
	}

	/** Main method for testing.  Every class can have a main
	 * method in Java */
	public static void main(String[] args)
	{
		// uncomment a call here to run a test
		// and comment out the ones you don't want
		// to run
		//testZeroBlue();
		//testKeepOnlyBlue();
		//testKeepOnlyRed();
		//testKeepOnlyGreen();
		//testNegate();
		//testGrayscale();
		//testFixUnderwater();
		//testMirrorVertical();
		//testMirrorVerticalRightToLeft();
		//testMirrorHorizontal();
		//testMirrorHorizontalBotToTop();
		//testMirrorDiagonal();
		//testMirrorTemple();
		//testMirrorArms();
		//testMirrorGull();
		//testMirrorDiagonal();
		//testCollage();
		//testCopy();
		//testMyCollage();
		testEdgeDetection();
		//testEdgeDetection2();
		//testChromakey();
		//testEncodeAndDecode();
		//testGetCountRedOverValue(250);
		//testSetRedToHalfValueInTopHalf();
		//testClearBlueOverValue(200);
		//testGetAverageForColumn(0);
	}
}