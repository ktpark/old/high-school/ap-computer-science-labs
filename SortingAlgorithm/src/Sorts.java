
import java.util.*;

/**
 *  Description of the Class
 *
 *  @author Kevin Park
 *  Feb 9, 2015
 */
public class Sorts{
	private long steps;

	/**
	 * Default constructor that initializes the step variable to 0
	 *
	 * @param  list  Description of Parameter
	 */
	public Sorts(){
		steps = 0;
	}

	/**
	 * Sorts an array in a random order into an ascending order using the bubble sorting algorithm
	 * Goes through the array and if the object before is larger than the one coming after, swap it's position.
	 *
	 * @param  list  reference to an array of integers to be sorted
	 */
	public void bubbleSort(ArrayList <Comparable> list) {
		for(int i=0;i<list.size();i++) { //Loop for Each pass
			steps++;//size()
			for(int j=1;j<list.size()-i;j++) { //Loop for comparing adjacent
				steps++;//size()
				if(list.get(j-1).compareTo(list.get(j))>0) //Compare adjacent
					swap(list, j-1, j); //Swap for correct positioning
				steps += 3; //For issuing get()
			}
		}
		
		//PREVIOUS
		/*while(steps<list.size()-1) { //Loop that loops size-1
			boolean isSwap = false; //Variable that checks if i need to step
			for(int j=0;j<list.size()-steps-1;j++) { //Loop through uncertain parts in the array
				if(list.get(j).compareTo(list.get(j+1))>0) { //Is the following object before t
					swap(list, j, j+1); //swap two positions
					isSwap = true; //let the variable know to increment step
				}
			}
			if(!isSwap) //If nothing changed
				break; //Finish
			//System.out.println(list);
			steps++; //Increment noting the change in the array
		}*/
	}

	/**
	 * Sorts an array in a random order into an ascending order using the selection sorting algorithm
	 * Selects the obejcts that comes the latest and replace it with the end. Each loop reduce the range to be checked by 1.
	 *
	 * @param  list  reference to an array of integers to be sorted
	 */
	public void selectionSort(ArrayList <Comparable> list){
		for(int i=0;i<list.size();i++) { //Loop for Each pass
			steps++;//size()
			int c = i; //Index of smallest
			for(int j=i+1;j<list.size();j++) { //Loop in search of smallest
				steps++;//size()
				if(list.get(c).compareTo(list.get(j))>0) //If smaller than current
					c=j; //Set new smallest index
				steps+=3; //Two get()
			}
			swap(list, i, c); //Swap with the current smallest
		}
		
		//PREVIOUS
		/*int loop = 0;
		while(loop<list.size()-1) { //Loop that loops size-1
			boolean isSwap = true; //variable that affects step
			int place = 0; //position of the current largest object
			Comparable t = list.get(0); //Starting object
			for(int i=1;i<list.size()-loop;i++) {
				if(t.compareTo(list.get(i))<0) { //If another object is bigger
					t = list.get(i); //Set t as new variable to be used
					place = i; //Store the position that t came from
					if(i==list.size()-loop-1)
						isSwap = false; //note to not to increment step
				}
			}
			swap(list, place,(int)(list.size()-1-loop)); //swap the largest object with the correct position
			loop++; //Increment the while loop counter
			if(!isSwap)
				continue;
			//System.out.println(list);
			steps++; //Increment noting the change in the array
		}*/
	}

	/**
	 * Sorts an array in a random order into an ascending order using the insertion sorting algorithm
	 * Checks each object inside the array and insert them in the correct position
	 *
	 * @param  list  reference to an array of integers to be sorted
	 */
	public void insertionSort(ArrayList <Comparable> list){
		for(int i=1;i<list.size();i++) { //Loop for Each pass\
			steps++;//size()
			int j=i; //Index to be sorted
			while(j>0 && list.get(j).compareTo(list.get(j-1))<0) { //Loop until correct position
				steps+=3; //Two get()
				swap(list, j, j-1); //Swap with adjacent
				j--; //Descend down in order
			}
		}
		
		//PREVIOUS
		/*int loop = 0; //Counter for the while loop
		while(loop<list.size()-1) { //Loop that loops size-1
			Comparable t = list.get((int)loop+1);
			for(int i=0;i<=loop;i++) { //Go through availabe spaces
				if(list.get(i).compareTo(t)>0) { //If the position is found
					list.remove((int)loop+1); //remove object from its position
					list.add(i, t); //place it in a new position and shift others
					//System.out.println(list);
					steps++; //increment the step
					break; //break out of the loop once found its position
				}
			}
			loop++; //Increment the counter by 1
		}*/
	}


	/**
	 *  Takes in entire vector, but will merge the following sections
	 *  together:  Left sublist from a[first]..a[mid], right sublist from
	 *  a[mid+1]..a[last].  Precondition:  each sublist is already in
	 *  ascending order
	 *
	 * @param  a      reference to an array of integers to be sorted
	 * @param  first  starting index of range of values to be sorted
	 * @param  mid    midpoint index of range of values to be sorted
	 * @param  last   last index of range of values to be sorted
	 */
	private void merge(ArrayList <Comparable> a, int first, int mid, int last){
		ArrayList<Comparable> t = (ArrayList)a.clone();steps++; //Clone original as helper
		int f=first, l=mid+1, c=first; //Counter variables
		while(f<=mid&&l<=last) { //While both are not empty
			steps+=3;//Two get() below and a compareTo
			if(t.get(f).compareTo(t.get(l))>0) { //Compare two from each array
				a.set(c,t.get(l)); //One from 2nd array is smaller
				steps+=2;//One for a.set() and one for t.get()
				l++; //Increment the 2nd array counter
			} else {
				a.set(c, t.get(f)); //One from 1st array is smaller
				steps+=2;//One for a.set() and one for t.get()
				f++; //Increment the 1st array counter
			}
			c++; //Increment positioning counter
		}
		while(f<=mid) { //Fill in remainders from 1st array
			a.set(c, t.get(f));
			//One for a.set() and one for t.get()
			f++; c++;
		}
	}

	/**
	 *  Recursive mergesort of an array of integers
	 *
	 * @param  a      reference to an array of integers to be sorted
	 * @param  first  starting index of range of values to be sorted
	 * @param  last   ending index of range of values to be sorted
	 */
	public void mergeSort(ArrayList <Comparable> a, int first, int last){
		if(last-first!=0) { //Stop recursive when it's by itself
			mergeSort(a, first, first+(last-first)/2); //if 0-3 -> 0-1, 2-3 //0-2 -> 0,1 2,2
			mergeSort(a, (first+(last-first)/2)+1, last); //4-6 -> 4-5, 6,6 //3-5 -> 3,4 5,5
			merge(a, first, first+(last-first)/2, last); //Merge two together
		}
		
		//PREVIOUS
		/*for(int i=first;i<last;i++)
			if(a.get(i).compareTo(a.get(i+1))>0)
				swap(a, i, i+1);*/
	}


	/**
	 *  Accessor method to return the current value of steps
	 *
	 */
	public long getStepCount(){
		return steps;
	}

	/**
	 *  Modifier method to set or reset the step count. Usually called
	 *  prior to invocation of a sort method.
	 *
	 * @param  stepCount   value assigned to steps
	 */
	public void setStepCount(long stepCount){
		steps = stepCount;
	}

	/**
	 *  Interchanges two elements in an ArrayList
	 *
	 * @param  list  reference to an array of integers
	 * @param  a     index of integer to be swapped
	 * @param  b     index of integer to be swapped
	 */
	public void swap(ArrayList <Comparable> list, int a, int b){
		Comparable a1 = list.get(a); steps++;//Store first object in tempoary variable
		list.set(a, list.get(b)); steps++;//Replace position a with object b
		list.set(b, a1); steps++;//Replace b position with temp object
	}
}
